require('../css/app.css');
window.onload = function () {
    $("body .icon-save").on("click", $(".icon-save"), function (event) {
        let textInBlockResalt = $(this).next().select();
        document.execCommand("copy");
        console.log("textInBlockResalt" + textInBlockResalt);
    });
};
$('body').on('click', "#addTeplate", function (event) {
    let templateText = $('#templateInput').val();
    let templateTableBody = $('#templateTableBody');
    let templatesStatus = $("#selectTable option:selected").attr('templateStatus');

    $.ajax({
        url: "/add-template",
        type: 'post',
        data: {
            'templateText': templateText,
            'status': templatesStatus
        },
        success: function (data) {
            templateTableBody.append('<tr templateId="' + data + '">' +
                '<td>' + data + ' </td>' +
                '<td >' + templateText + ' </td>' +
                '<td>0</td>' +
                '<td><input type="submit" value="X" deleteTemplate="' + data + '"></td>' +
                '</tr>');
        }
    })
});
$('body').on('click', "[deleteTemplate]", function (event) {
    let templateId = $(this).attr('deleteTemplate');
    $.ajax({
        url: "/delete-template",
        type: 'post',
        data: 'templateId=' + templateId,
        success: function (data) {
            let tableRow = $('[templateId=' + templateId + ']');
            tableRow.remove();
        }
    })
});
$('body').on('change', '#selectTable', function (event) {
    let templatesStatus = $("#selectTable option:selected").attr('templateStatus');
    let templateTableBody = $('#templateTableBody');
    let templateTableName = $('#templateTableName');
    templateTableBody.empty()
    $.ajax({
        url: "/get-by-status",
        type: 'post',
        data: 'status=' + templatesStatus,
        success: function (data) {
            data.forEach(function (item) {
                templateTableBody.append('<tr templateId="' + item.id + '">\n' +
                    '<td>' + item.id + '</td>\n' +
                    '<td templateText>' + item.text + '</td>\n' +
                    '<td>' + item.count + '</td>\n' +
                    '<td><input type="submit" value="X" deleteTemplate="' + item.id + '"></td>\n' +
                    '</tr>');
            })
            if (templatesStatus == 1) {
                templateTableName.text('After');
            } else {
                templateTableName.text('Before');
            }
        }
    })
});
function incrementCount(templateId) {
    $.ajax({
        url: "/increment-counter",
        type: 'post',
        data: 'templateId=' + templateId,
        success: function (data) {
            return data;
        }
    })
}
function getTemplatesByStatus(status) {
    return $.ajax({
        url: "/get-by-status-for-gen",
        type: 'post',
        data: {
            'status': status
        },
        async: false,

    }).responseJSON
}

$('#startGenerate').on('click', function (event) {
    let mainLink = $('#mainLink').val();
    let phrasesArr = $('#phrases').val().split('\n');
    let linksArr = $('#links').val().split('\n');
    let resultOutput = document.getElementById("resultOutput");

    let templatesBeforeArray = $.map(getTemplatesByStatus(0), function (el) {
        return el
    });
    let templatesAftereArray = $.map(getTemplatesByStatus(1), function (el) {
        return el
    });

    resultOutput.innerHTML = "";
    linksArr.forEach(function (item, i) {
        if(item != ''){
            let template = '';
            let phraseOrLink;
            if (phrasesArr[i] !== undefined && phrasesArr[i] !== '') {
                template = templatesBeforeArray[Math.floor(Math.random() * templatesBeforeArray.length)]['text'];
                phraseOrLink = function(string){return string.replace('{text}',phrasesArr[i])};

            } else {
                template = templatesAftereArray[Math.floor(Math.random() * templatesAftereArray.length)]['text'];
                phraseOrLink = function(string){return string.replace('{link}', mainLink)};
            }
            let resString = choiceRandomBracketsWord(template);

            resString = resString.replace('{site}', item);
            resString = phraseOrLink(resString);
            resString = resString.replace('+', '');
            resultOutput.append($.trim(resString) + "\r");
        }
    })
});

function choiceRandomBracketsWord(template) {
    let wordBracketArr = fromBracketsToArray(template);
    let resString = template;

    if(Array.isArray(wordBracketArr)){
        for (let j = 0; j < wordBracketArr.length; j++) {
            resString = resString.replace(template.match(/\(([^)]+)\)/g)[j], wordBracketArr[j][Math.floor(Math.random() * wordBracketArr[j].length)]);
        }
        return resString;
    }
    return template;
}

function fromBracketsToArray(template) {
    let resultWordsInBrackets = template.match(/\(([^)]+)\)/g);
    let wordBracketArr = [];

    if(resultWordsInBrackets !== null) {
        resultWordsInBrackets.forEach(function (item, i) {
            item = item.replace(/[{()}]/g, '');
            wordBracketArr[i] = item.split('|');
        });
        return wordBracketArr;
    }
    return template
}


