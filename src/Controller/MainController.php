<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 05.03.2018
 * Time: 10:44
 */

namespace App\Controller;


use App\Entity\Templates;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MainController extends Controller {

	/**
	 * @Route("/")
	 */
	public function index() {
		$templates = $this->getDoctrine()
		                  ->getRepository( Templates::class )
		                  ->findBy( [ 'status' => 0 ] );

		return $this->render( 'index/index.html.twig', [
			'templates' => $templates
		] );
	}

	/**
	 * @Route("/add-template" , methods={"POST"})
	 */
	public function addTemplate( Request $request ) {
		$templateText = $request->get( 'templateText' );
		$status = $request->get( 'status' );

		$em       = $this->getDoctrine()->getManager();
		$template = new Templates();
		$template->setText( $templateText );
		$template->setCount( 0 );
		$template->setStatus( $status );

		$em->persist( $template );

		$em->flush();

		return $this->json( $template->getId() );
	}

	/**
	 * @Route("/delete-template", methods={"POST"} )
	 */
	public function deleteTemplate( Request $request ) {
		$templateId = $request->get( 'templateId' );
		$em         = $this->getDoctrine()->getManager();
		$template   = $em->getRepository( 'App:Templates' )->find( $templateId );

		$em->remove( $template );
		$em->flush();

		return $this->json( $template->getId() );
	}

	/**
	 * @Route("/increment-counter", methods={"POST"} )
	 */
	public function incrementCounter( Request $request ) {
		$templateId   = $request->get( 'templateId' );
		$em           = $this->getDoctrine()->getManager();
		$template     = $em->getRepository( 'App:Templates' )->find( $templateId );
		$currentCount = $template->getCount();

		$template->setCount( $currentCount + 1 );
		$em->persist( $template );
		$em->flush();

		return $this->json( $template->getCount() );
	}

	/**
	 * @Route("/get-by-status", methods={"POST"} )
	 */
	public function getTemplatesByStatus( Request $request ) {
		$status         = $request->get( 'status' );
		$templates      = $this->getDoctrine()->getRepository( Templates::class )->findBy(['status' => $status]);

		$response       = array();
		foreach ( $templates as $item ) {
			$response[] = [
				'id'    => $item->getId(),
				'text'  => $item->getText(),
				'count' => $item->getCount()
			];
		}
		return $this->json( $response );
	}

	/**
	 * @Route("/get-by-status-for-gen", methods={"POST"} )
	 */
	public function getByStatusForGen( Request $request ) {
		$status         = $request->get( 'status' );
		$templates      = $this->getDoctrine()->getRepository( Templates::class )->findBy(['status' => $status]);

		$response       = array();
		foreach ( $templates as $item ) {
			$response[] = [
				'text'  => $item->getText(),
			];
		}
		return $this->json( $response );
	}
}