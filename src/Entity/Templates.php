<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TemplatesRepository")
 */
class Templates
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $text;

	/**
	 * @ORM\Column(type="integer", options={"default" : 0})
	 */
    private $count;


	/**
	 * @ORM\Column(type="boolean")
	 */
    private $status;
	/**
	 * @return mixed
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function setId( $id ): void {
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getText() {
		return $this->text;
	}

	/**
	 * @param mixed $text
	 */
	public function setText( $text ): void {
		$this->text = $text;
	}

	/**
	 * @return mixed
	 */
	public function getCount() {
		return $this->count;
	}

	/**
	 * @param mixed $count
	 */
	public function setCount( $count ): void {
		$this->count = $count;
	}

	/**
	 * @return mixed
	 */
	public function getStatus() {
		return $this->status;
	}

	/**
	 * @param mixed $count
	 */
	public function setStatus( $status ): void {
		$this->status = $status;
	}

}
